# Docker Configuration
## Mounting
Mount your source location to the `/src` directory   
`-v ./:/src`   
# Debian
## Software
* Git
* libpng-dev
* libxml2-dev
* nodejs
* zip
# Apache
## Modules
* Enabled mod_rewrite
## Public
Apache is setup to host files in the directory `/src/public`
# PHP
## Extensions
* gd
* pdo
* pdo_mysql
* xml
## Composer
Composer is added however nothing is setup or installed globally. It is just made available.
## PECL & PEAR
* Redis (pecl)
* Xdebug (pecl)