FROM php:8-apache

# Software
RUN curl -fsSL https://deb.nodesource.com/setup_17.x | bash - 
# RUN apt-get update
RUN apt-get install -y \
    git \
    libpng-dev \
    libxml2-dev \
    nodejs \
    zip
# PHP Setup
RUN docker-php-ext-install \
    gd \
    pdo \
    pdo_mysql \
    xml
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
  && php composer-setup.php \
  && php -r "unlink('composer-setup.php');" \
  && chmod +x composer.phar \
  && mv composer.phar /usr/bin/composer
RUN pecl install redis \
    && pecl install xdebug \
    && docker-php-ext-enable redis 
COPY ./php/conf.d/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
COPY ./php/conf.d/error_reporting.ini /usr/local/etc/php/conf.d/error_reporting.ini

# Apache Setup
RUN a2enmod rewrite
ENV APACHE_DOCUMENT_ROOT /src/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf